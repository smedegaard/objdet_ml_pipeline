import tensorflow as tf
import tensorflow_transform as tft

from utils.utils import _transformed_name


@tf.function
def process_image(image):
    img = tf.io.decode_jpeg(image[0], channels=3)
    resized_img = tf.image.resize_with_crop_or_pad(
        img, target_height=512, target_width=512
    )
    resized_img = tf.cast(resized_img, tf.float32)
    # normalize to [-1,1] for
    # https://tfhub.dev/tensorflow/efficientdet/lite3/feature-vector/1
    normalized_img = resized_img / 127.5
    normalized_img = normalized_img - 1.0
    return normalized_img
    # return tf.reshape(normalized_img, [-1, 512, 512, 3])


def preprocessing_fn(tf_record: map) -> map:
    """tf.transform's callback function for preprocessing tf_record.
    Args:
        tf_record: map from feature keys to raw not-yet-transformed features.
    Returns:
        Map from string feature key to transformed feature operations.
    """
    _IMAGE_KEY = "image/encoded"
    _LABEL_KEY = "image/object/class/label"
    _LABEL_NAME = "image/object/class/text"
    _YMIN = "image/object/bbox/ymin"
    _YMAX = "image/object/bbox/ymax"
    _XMIN = "image/object/bbox/xmin"
    _XMAX = "image/object/bbox/xmax"

    outputs = {}

    # tf.io.decode_jpeg function cannot be applied on a batch of data.
    # We have to use tf.map_fn
    image_features = tf.map_fn(
        lambda image: process_image(image),
        tf_record[_IMAGE_KEY],
        dtype=tf.float32,
    )

    # image_features = tf.keras.applications.mobilenet.preprocess_input(image_features)

    # outputs[_transformed_name(_IMAGE_KEY)] = image_features
    outputs["input_1"] = image_features
    outputs[_transformed_name(_LABEL_KEY)] = tf_record[_LABEL_KEY]
    outputs[_transformed_name(_LABEL_NAME)] = tf_record[_LABEL_NAME]
    outputs[_transformed_name(_YMIN)] = tf_record[_YMIN]
    outputs[_transformed_name(_YMAX)] = tf_record[_YMAX]
    outputs[_transformed_name(_XMIN)] = tf_record[_XMIN]
    outputs[_transformed_name(_XMAX)] = tf_record[_XMAX]

    return outputs
