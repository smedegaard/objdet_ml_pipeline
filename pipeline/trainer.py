from typing import List, Text
from absl import logging
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow_transform as tft
from tensorflow_transform.tf_metadata import schema_utils

from tfx.components.trainer.executor import TrainerFnArgs
from tfx.components.trainer.fn_args_utils import DataAccessor

# from models import constants
# from models import features
from tfx.utils import io_utils
from tfx_bsl.tfxio import dataset_options

from tensorflow_metadata.proto.v0 import schema_pb2

import tensorflow_hub as hub

# def _get_serve_image_fn(model, tf_transform_output):
#     """Returns a function that feeds the input tensor into the model."""

#     @tf.function
#     def serve_image_fn(serialized_tf_examples):
#         """Returns the output to be used in the serving signature.
#         """

#         feature_spec = tf_transform_output.raw_feature_spec()
#         feature_spec.pop(odp.configs._LABEL_KEY)
#         parsed_features = tf.io.parse_example(serialized_tf_examples, feature_spec)

#         transformed_features = model.tft_layer(parsed_features)
#         transformed_features[
#             model.get_layer("mobilenet_1.00_224").layers[0].name
#         ] = transformed_features[odp.utils._transformed_name(odp.utils._IMAGE_KEY)]
#         del transformed_features[odp.utils._transformed_name(odp.configs._IMAGE_KEY)]

#         return model(transformed_features)

#     return serve_image_fn


def _input_fn(
    file_pattern: List[Text],
    data_accessor: DataAccessor,
    schema: schema_pb2.Schema,
    batch_size: int = 1,
) -> tf.data.Dataset:
    """Generates features and label for tuning/training.

    Args:
      file_pattern: List of paths or patterns of input tfrecord files.
      data_accessor: DataAccessor for converting input to RecordBatch.
      schema: A schema proto of input data.
      batch_size: representing the number of consecutive elements of returned
        dataset to combine in a single batch

    Returns:
      A dataset that contains (features, indices) tuple where features is a
        dictionary of Tensors, and indices is a single Tensor of label indices.
    """
    return data_accessor.tf_dataset_factory(
        file_pattern,
        dataset_options.TensorFlowDatasetOptions(batch_size=batch_size),
        schema,
    ).repeat()


def _build_keras_model() -> tf.keras.Model:
    """
    Returns:
      A Keras Model.
    """
    do_fine_tuning = False
    IMAGE_SIZE = (512, 512)
    NUM_CLASSES = 3
    # model_handle = "https://tfhub.dev/tensorflow/centernet/resnet50v2_512x512/1"
    # model_handle = "https://tfhub.dev/tensorflow/efficientnet/b2/feature-vector/1"
    # model_handle = "https://tfhub.dev/tensorflow/efficientdet/d0/1"
    model_handle = "https://tfhub.dev/tensorflow/efficientdet/lite3/feature-vector/1"
    # model = tf.keras.Sequential(
    #     [
    #         # Explicitly define the input shape so the model can be properly
    #         # loaded by the TFLiteConverter
    #         tf.keras.layers.InputLayer(
    #             input_shape=IMAGE_SIZE + (3,), name="image_input"
    #         ),
    #         hub.KerasLayer(model_handle, trainable=do_fine_tuning),
    #         tf.keras.layers.Dropout(rate=0.2),
    #         tf.keras.layers.Dense(
    #             NUM_CLASSES, kernel_regularizer=tf.keras.regularizers.l2(0.0001)
    #         ),
    #     ]
    # )

    # model.compile(
    #     optimizer=keras.optimizers.adam(1e-2),
    #     loss=tf.keras.losses.sparsecategoricalcrossentropy(from_logits=true),
    #     metrics=[keras.metrics.sparsecategoricalaccuracy()],
    # )
    img_inputs = keras.Input(shape=IMAGE_SIZE + (3,))
    hub_model = hub.KerasLayer(model_handle, trainable=do_fine_tuning)
    outputs = hub_model(img_inputs)
    model = keras.Model(inputs=img_inputs, outputs=outputs, name="obj_det_model")

    model.compile()

    # model.build((None,) + IMAGE_SIZE + (3,))
    model.summary(print_fn=logging.info)
    return model


def run_fn(fn_args: TrainerFnArgs):
    # schema = io_utils.parse_pbtxt_file(fn_args.schema_file, schema_pb2.Schema())
    # feature_list = features.FEATURE_KEYS
    # label_key = features.LABEL_KEY
    # mirrored_strategy = tf.distribute.MirroredStrategy()
    # train_batch_size = (
    #     constants.TRAIN_BATCH_SIZE * mirrored_strategy.num_replicas_in_sync
    # )
    # eval_batch_size = constants.EVAL_BATCH_SIZE * mirrored_strategy.num_replicas_in_sync

    tf_transform_output = tft.TFTransformOutput(fn_args.transform_output)

    # Train and eval files contains transformed examples.
    # _input_fn read dataset based on transformed schema from tft.
    train_dataset = _input_fn(
        fn_args.train_files,
        fn_args.data_accessor,
        tf_transform_output.transformed_metadata.schema,
    )
    eval_dataset = _input_fn(
        fn_args.eval_files,
        fn_args.data_accessor,
        tf_transform_output.transformed_metadata.schema,
    )

    # with mirrored_strategy.scope():
    model = _build_keras_model()

    # Write logs to path
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
        log_dir=fn_args.model_run_dir, update_freq="batch"
    )

    model.fit(
        train_dataset,
        steps_per_epoch=fn_args.train_steps,
        validation_data=eval_dataset,
        validation_steps=fn_args.eval_steps,
        callbacks=[tensorboard_callback],
    )

    # signatures = {
    #     "serving_default": _get_serve_image_fn(model).get_concrete_function(
    #         tf.TensorSpec(
    #             shape=[None, 512, 512, 3],
    #             dtype=tf.float32,
    #             name="image/encoded_xf",
    #         )
    #     )
    # }

    # signatures = {
    #     "serving_default": _get_serve_tf_examples_fn(
    #         model, schema, tf_transform_output
    #     ).get_concrete_function(
    #         tf.TensorSpec(shape=[None], dtype=tf.string, name="examples")
    #     ),
    # }

    model.save(fn_args.serving_model_dir, save_format="tf")
